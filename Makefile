test:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine go test

format:
	@docker run --rm -v $(CURDIR):/app -w /app golang:1.8-alpine gofmt -w /app
