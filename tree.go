package bst

type Tree interface {
	Insert(Tree) Tree
	LookUp(int) Tree
	ApplyInOrder(TreeFunc, string)
	ApplyPostOrder(TreeFunc, string)
	GetName() string
	SetName(string)
	GetValue() int
	SetLeft(Tree)
	GetLeft() Tree
	SetRight(Tree)
	GetRight() Tree
}

type TreeFunc func(Tree, string)

type Node struct {
	name  string
	val   int
	left  Tree
	right Tree
}

func NewNode(name string, val int) Tree {
	node := Node{
		name: name,
		val:  val,
	}

	return &node
}

func (n *Node) GetValue() int {
	return n.val
}

func (n *Node) GetName() string {
	return n.name
}

func (n *Node) SetName(name string) {
	n.name = name
}

func (n *Node) SetLeft(l Tree) {
	n.left = l
}

func (n *Node) SetRight(r Tree) {
	n.right = r
}

func (n *Node) GetRight() Tree {
	return n.right
}

func (n *Node) GetLeft() Tree {
	return n.left
}

func InsertToTree(treep Tree, newp Tree) Tree {
	if treep == nil {
		return newp
	}

	cmpRes := CompareValues(newp.GetValue(), treep.GetValue())
	if cmpRes < 0 {
		treep.SetLeft(InsertToTree(treep.GetLeft(), newp))
	}

	if cmpRes > 0 {
		treep.SetRight(InsertToTree(treep.GetRight(), newp))
	}

	return treep
}

// Insert new Node in Tree
func (treep *Node) Insert(newp Tree) Tree {

	cmpRes := CompareValues(newp.GetValue(), treep.GetValue())
	if cmpRes < 0 {
		if treep.GetLeft() == nil {
			treep.SetLeft(newp)
		} else {
			treep.SetLeft(treep.GetLeft().Insert(newp))
		}
	}

	if cmpRes > 0 {
		if treep.GetRight() == nil {
			treep.SetRight(newp)
		} else {
			treep.SetRight(treep.GetRight().Insert(newp))
		}
	}

	return treep
}

// Return Node By required val
func (treep *Node) LookUp(val int) Tree {
	var cmp int

	cmp = CompareValues(treep.GetValue(), val)

	if cmp == 0 {
		return treep
	}

	if cmp > 0 && treep.GetLeft() != nil {
		return treep.GetLeft().LookUp(val)
	}

	if cmp < 0 && treep.GetRight() != nil {
		return treep.GetRight().LookUp(val)
	}

	return nil
}

//
func (treep *Node) ApplyInOrder(void TreeFunc, arg string) {
	if treep.GetLeft() != nil {
		treep.GetLeft().ApplyInOrder(void, arg)
	}
	void(treep, arg)

	if treep.GetRight() != nil {
		treep.GetRight().ApplyInOrder(void, arg)
	}
}

func (treep *Node) ApplyPostOrder(void TreeFunc, arg string) {
	if treep.GetLeft() != nil {
		treep.GetLeft().ApplyInOrder(void, arg)
	}

	if treep.GetRight() != nil {
		treep.GetRight().ApplyInOrder(void, arg)
	}

	void(treep, arg)
}

func CompareValues(fst int, sec int) int {
	if fst > sec {
		return 1
	}

	if fst < sec {
		return -1
	}

	return 0
}
